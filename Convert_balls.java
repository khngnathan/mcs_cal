import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Convert_balls {

	public static void main(String[] args) throws IOException {

		// TODO Auto-generated method stub
		String output = "";
		int label_group_num = 1; 	//group how many node labels into one

		ArrayList<Integer> unique_node_labels = new ArrayList<>();
		ArrayList<Node_mapping_pair> node_mapping_list = new ArrayList<>();

		for (int i = 1; i <= 1000; i++) {
			ArrayList<String> all_node_line = new ArrayList<>();
			ArrayList<String> all_edge_line = new ArrayList<>();

			ArrayList<Integer> node_list = new ArrayList<>();
			ArrayList<Integer> node_label_list = new ArrayList<>();

			ArrayList<String> node_list_output = new ArrayList<>();
			ArrayList<String> edge_list_output = new ArrayList<>();

			String each_ball = "t # " + (i - 1) + "\n";	//first line of each ball's output
			//System.out.println(each_ball);
			
			// load in each file
			File file = new File("C:\\Users\\cskhng\\Desktop\\dataset\\ball_No_" + i + ".txt");
			BufferedReader br = new BufferedReader(new FileReader(file));

			String each_line;
			while ((each_line = br.readLine()) != null) {
				//System.out.println(each_line);
				if (each_line == "") {
					continue;
				}
				String[] all_components = each_line.split(" ");
				if (all_components.length == 2) {
					all_node_line.add(each_line);		//this line is a node
				} else if (all_components.length == 3) {
					all_edge_line.add(each_line);		//this line is an edge
				}
			}

			//process each line that indicates a node
			for (int j = 0; j < all_node_line.size(); j++) {
				String each_node = all_node_line.get(j);
				//System.out.println(each_node);

				String[] all_components = each_node.split(" ");
				String n_id_1 = all_components[0];
				String n_label = all_components[1];
//
//				System.out.print("n_1: " + n_id_1);
//				System.out.println(", n_2: " + n_id_2);

				boolean n_1_exist = false;

				for (int k = 0; k < node_list.size(); k++) {
					if (Integer.parseInt(n_id_1) == node_list.get(k)) {
						n_1_exist = true;
					}
				}
				if (!n_1_exist) {
					node_list.add(Integer.parseInt(n_id_1));
					node_label_list.add(Integer.parseInt(n_label));
				}
			}
			
			//group multiple node labels into 1
			for (int j = 0; j < node_label_list.size(); j++) {
				int this_label = node_label_list.get(j);
				boolean already_recorded = false;
				for (int k = 0; k < unique_node_labels.size(); k++) {
					if(this_label == unique_node_labels.get(k)) {
						already_recorded = true;
						break;
					}
				}
				if(!already_recorded) {
					unique_node_labels.add(this_label);
				}
			}
			System.out.println("unique_node_labels: " + unique_node_labels);
			
			ArrayList<Integer> current_mapping_list = new ArrayList<>();
			int current_node_label = 0;
			for(int j = 0; j < unique_node_labels.size(); j++) {
				if(j % label_group_num == 0 && j != 0) {
					node_mapping_list.add(new Node_mapping_pair(current_mapping_list,current_node_label));
					current_mapping_list = new ArrayList<>();
					current_node_label++;
				}
				current_mapping_list.add(unique_node_labels.get(j));
			}
			if(current_mapping_list.size()!= 0) {
				node_mapping_list.add(new Node_mapping_pair(current_mapping_list,current_node_label));
			}
			
			
			
		}
		for(int j = 0; j < node_mapping_list.size(); j++) {
			System.out.println(node_mapping_list.get(j).list + "," + node_mapping_list.get(j).new_label);
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		for (int i = 1; i <= 1000; i++) {
			ArrayList<String> all_node_line = new ArrayList<>();
			ArrayList<String> all_edge_line = new ArrayList<>();

			ArrayList<Integer> node_list = new ArrayList<>();
			ArrayList<Integer> node_label_list = new ArrayList<>();

			ArrayList<String> node_list_output = new ArrayList<>();
			ArrayList<String> edge_list_output = new ArrayList<>();

			String each_ball = "t # " + (i - 1) + "\n";	//first line of each ball's output
			//System.out.println(each_ball);
			
			// load in each file
			File file = new File("C:\\Users\\cskhng\\Desktop\\dataset\\ball_No_" + i + ".txt");
			BufferedReader br = new BufferedReader(new FileReader(file));

			String each_line;
			while ((each_line = br.readLine()) != null) {
				//System.out.println(each_line);
				if (each_line == "") {
					continue;
				}
				String[] all_components = each_line.split(" ");
				if (all_components.length == 2) {
					all_node_line.add(each_line);		//this line is a node
				} else if (all_components.length == 3) {
					all_edge_line.add(each_line);		//this line is an edge
				}
			}

			//process each line that indicates a node
			for (int j = 0; j < all_node_line.size(); j++) {
				String each_node = all_node_line.get(j);
				//System.out.println(each_node);

				String[] all_components = each_node.split(" ");
				String n_id_1 = all_components[0];
				String n_label = all_components[1];
//
//				System.out.print("n_1: " + n_id_1);
//				System.out.println(", n_2: " + n_id_2);

				boolean n_1_exist = false;

				for (int k = 0; k < node_list.size(); k++) {
					if (Integer.parseInt(n_id_1) == node_list.get(k)) {
						n_1_exist = true;
					}
				}
				if (!n_1_exist) {
					node_list.add(Integer.parseInt(n_id_1));
					node_label_list.add(Integer.parseInt(n_label));
				}
			}
			
		
			
			//construct output line
			//System.out.println("# of nodes: " + node_list.size());
			for(int k = 0; k < node_list.size(); k++) {
				int node_id = node_list.get(k);
				int node_label = node_label_list.get(k);
				
				int new_node_label = -1;
				for(int j = 0; j < node_mapping_list.size(); j++) {
					ArrayList<Integer> old_label_list = node_mapping_list.get(j).list;
					for(int q = 0; q < old_label_list.size(); q++) {
						if(old_label_list.get(q) == node_label) {
							new_node_label = node_mapping_list.get(j).new_label;
						}
					}
				} 
				String this_node_string = "v " + k + " " + new_node_label;
				node_list_output.add(this_node_string);
				//System.out.println(node_list_output.get(k));
			}
			
			
			
			
			
			
			for(int k = 0; k < all_edge_line.size(); k++) {
//				System.out.println(all_edge_line.get(k));
				
				String [] all_components = all_edge_line.get(k).split(" ");
				String n_id_1 = all_components[0];
				String n_id_2 = all_components[1];

				int new_n1 = -1;
				int new_n2 = -1;
				for(int q = 0; q < node_list.size(); q++) {
					if(Integer.parseInt(n_id_1) == node_list.get(q)) {
						new_n1 = q;
					}
					if(Integer.parseInt(n_id_2) == node_list.get(q)) {
						new_n2 = q;
					}
					
				}
				String this_edge = "";
				if(new_n1 < new_n2) {
					this_edge = "e " + new_n1 + " " + new_n2 + " 1";
				}else {
					this_edge = "e " + new_n2 + " " + new_n1 + " 1";
				}
				//System.out.println(this_edge);
				edge_list_output.add(this_edge);
			}
			
			//construct balls from the above lists here
			for(int k = 0; k < node_list_output.size(); k++) {
				each_ball += node_list_output.get(k);
				each_ball += "\n";
			}
			for(int k = 0; k < edge_list_output.size(); k++) {
				each_ball += edge_list_output.get(k);
				each_ball += "\n";
			}
			
			System.out.println(each_ball);
			
			//each_ball += "\n";
			output += each_ball;

			
		}

		
		
		//output fixed file
		String output_file_name = "balls_30_300_d_" + label_group_num + ".txt";
		System.out.println("outputing to file: " + output_file_name);
		
		FileOutputStream outputStream = new FileOutputStream(output_file_name);
	    byte[] strToBytes = output.getBytes();
	    outputStream.write(strToBytes);
	  
	    outputStream.close();
	}

}


class Node_mapping_pair{
	ArrayList<Integer> list;
	int new_label;
	
	Node_mapping_pair(ArrayList<Integer> list, int new_label) {
		this.list = list;
		this.new_label = new_label;
	}
	
}
